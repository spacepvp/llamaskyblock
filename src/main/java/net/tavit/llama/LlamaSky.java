package net.tavit.llama;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.Getter;
import net.taviit.core.User;
import net.taviit.core.Utilities.Logger;
import net.tavit.llama.Commands.CommandManager;
import net.tavit.llama.Commands.Commands;
import net.tavit.llama.Files.MessageFile;
import org.bson.Document;
import org.bukkit.plugin.java.JavaPlugin;

public class LlamaSky extends JavaPlugin {

    @Getter
    private static LlamaSky instance;

    /*
     * MongoDB
     */
    @Getter
    private MongoCollection<Document> planetCollection;
    @Getter
    private MongoDatabase db;
    @Getter
    private MongoClient mongoClient;

    /*
     * files
     */
    @Getter
    private MessageFile messageFile;

    /*
     * class instances
     */
    @Getter
    public static Commands commands;
    @Getter
    public CommandManager commandManager;

    @Override
    public void onEnable() {
        instance = this;

        registerFiles();
        loadMongo();
        loadCommands();
        loadEvents();
    }

    @Override
    public void onDisable() {

    }

    /*
     * load plugin commands
     */
    private void loadCommands() {
        commands = new Commands();
        commandManager = new CommandManager("planet");
        commandManager.registerCommands();
    }

    /*
     * load plugin events
     */
    private void loadEvents() {

    }

    /*
     * register plugin files
     */
    private void registerFiles() {
        saveDefaultConfig(); // save default config.yml

        /*
         * messages file
         */
        messageFile = new MessageFile(this);
    }

    /*
     * load mongo database
     */
    private void loadMongo() {

        String host = getConfig().getString("mongoDB.host");
        String port = getConfig().getString("mongoDB.port");
        String database = getConfig().getString("mongoDB.database");
        String user = getConfig().getString("mongoDB.user");
        String password = getConfig().getString("mongoDB.password");

        // connect to mongo
        mongoClient = new MongoClient(
                new MongoClientURI("mongodb://" + user + ":" + password + "@" + host + ":" + port));

        // get database defined in config
        db = mongoClient.getDatabase(database);
        planetCollection = db.getCollection("islands"); // get islands collection

        Logger.log("&aSuccessfully connected to database!"); // log connection
        Logger.log("&eLoading all planet data...");
        User.loadAllPlayersFromDatabase(planetCollection); // load all player data
    }

}
