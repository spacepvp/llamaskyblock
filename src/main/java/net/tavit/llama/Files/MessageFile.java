package net.tavit.llama.Files;

import net.tavit.llama.LlamaSky;

public class MessageFile extends FileHandler {
    public MessageFile(LlamaSky llamaSky) {
        super(llamaSky, "messages.yml");
    }
}
