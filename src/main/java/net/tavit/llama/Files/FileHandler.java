package net.tavit.llama.Files;

import net.tavit.llama.LlamaSky;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class FileHandler {

    protected LlamaSky main;
    private File file;
    public FileConfiguration config;

    public FileHandler(LlamaSky main, String filename) {
        this.main = main;
        this.file = new File(main.getDataFolder(), filename);
        if (!this.file.exists()) {
            try {
                this.file.createNewFile();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.config = YamlConfiguration.loadConfiguration(this.file);
    }

    public void save() {
        try {
            this.config.save(this.file);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void load() {
        this.config = YamlConfiguration.loadConfiguration(this.file);
    }

}
