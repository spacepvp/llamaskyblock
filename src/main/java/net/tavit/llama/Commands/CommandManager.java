package net.tavit.llama.Commands;

import net.tavit.llama.LlamaSky;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class CommandManager implements CommandExecutor, TabCompleter {

    public List<Command> commands = new ArrayList<>();

    public CommandManager(String command) {
        LlamaSky.getInstance().getCommand(command).setExecutor(this);
        LlamaSky.getInstance().getCommand(command).setTabCompleter(this);
    }

    public void registerCommands() {
        registerCommand(LlamaSky.getCommands().helpCommand);
    }

    public void registerCommand(Command command) {
        commands.add(command);
    }

    public void unRegisterCommand(Command command) {
        commands.remove(command);
    }

    @Override
    public boolean onCommand(CommandSender cs, org.bukkit.command.Command cmd, String s, String[] args) {
        try {
            if (args.length != 0) {
                for (Command command : commands) {
                    if (command.getAliases().contains(args[0]) && command.isEnabled()) {
                        if (command.isPlayer() && !(cs instanceof Player)) {
                            // Must be a player
                            // cs.sendMessage(Utils.color(IridiumSkyblock.getMessages().mustBeAPlayer.replace("%prefix%", IridiumSkyblock.getConfiguration().prefix)));
                            return true;
                        }
                        if ((cs.hasPermission(command.getPermission()) || command.getPermission().equalsIgnoreCase("") || command.getPermission().equalsIgnoreCase("iridiumskyblock.")) && command.isEnabled()) {
                            command.execute(cs, args);
                        } else {
                            // No permission
                            // cs.sendMessage(Utils.color(IridiumSkyblock.getMessages().noPermission.replace("%prefix%", IridiumSkyblock.getConfiguration().prefix)));
                        }
                        return true;
                    }
                }
            } else {
                if (cs instanceof Player) {
                    Player p = (Player) cs;
                    // User u = User.getUser(p);
                    /*if (u.getIsland() != null) {
                        if (u.getIsland().getSchematic() == null) {
                            if (IridiumSkyblock.getSchematics().schematics.size() == 1) {
                                for (Schematics.FakeSchematic schematic : IridiumSkyblock.getSchematics().schematics) {
                                    u.getIsland().setSchematic(schematic.name);
                                }
                            } else {
                                p.openInventory(u.getIsland().getSchematicSelectGUI().getInventory());
                                return true;
                            }
                        }
                        if (IridiumSkyblock.getConfiguration().islandMenu) {
                            p.openInventory(u.getIsland().getIslandMenuGUI().getInventory());
                        } else {
                            u.getIsland().teleportHome(p);
                        }
                    } else {
                        // IridiumSkyblock.getIslandManager().createIsland(p);
                    }*/

                    /*
                     * send help message
                     */

                    return true;
                }
            }
            // cs.sendMessage(Utils.color(IridiumSkyblock.getMessages().unknownCommand.replace("%prefix%", IridiumSkyblock.getConfiguration().prefix)));
        } catch (Exception e) {
            // IridiumSkyblock.getInstance().sendErrorMessage(e);
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender cs, org.bukkit.command.Command cmd, String s, String[] args) {
        try {
            if (args.length == 1) {
                ArrayList<String> result = new ArrayList<>();
                for (Command command : commands) {
                    for (String alias : command.getAliases()) {
                        if (alias.toLowerCase().startsWith(args[0].toLowerCase()) && (command.isEnabled() && (cs.hasPermission(command.getPermission()) || command.getPermission().equalsIgnoreCase("") || command.getPermission().equalsIgnoreCase("iridiumskyblock.")))) {
                            result.add(alias);
                        }
                    }
                }
                return result;
            }
            for (Command command : commands) {
                if (command.getAliases().contains(args[0]) && (command.isEnabled() && (cs.hasPermission(command.getPermission()) || command.getPermission().equalsIgnoreCase("") || command.getPermission().equalsIgnoreCase("iridiumskyblock.")))) {
                    return command.TabComplete(cs, cmd, s, args);
                }
            }
        } catch (Exception e) {
            // IridiumSkyblock.getInstance().sendErrorMessage(e);
        }
        return null;
    }
}
