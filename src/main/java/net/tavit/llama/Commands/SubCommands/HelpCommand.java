package net.tavit.llama.Commands.SubCommands;

import net.tavit.llama.Commands.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.List;

public class HelpCommand extends Command {

    public HelpCommand() {
        super(Arrays.asList("help", "?"), "Show planet help menu", "", false);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        // sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8Plugin Name: &7IridiumSkyblock"));
        // sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8Plugin Version: &7" + IridiumSkyblock.getInstance().getDescription().getVersion()));
        // sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8Coded by IridiumLLC"));
        if(sender instanceof Player) {
            final Player player = (Player) sender;
            player.sendMessage("planet help message here");
            return;
        }
        sender.sendMessage("You must be a player to use this command.");
    }

    @Override
    public void admin(CommandSender sender, String[] args) {
        execute(sender, args);
    }

    @Override
    public List<String> TabComplete(CommandSender cs, org.bukkit.command.Command cmd, String s, String[] args) {
        return null;
    }

}
